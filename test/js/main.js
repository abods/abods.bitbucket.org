
var app = angular.module('colorQtest', ['ngRoute','ngAnimate'])

// VARS
.constant({
	'AppSettings':{
		api: {
			version: 'v1',
			url: 'http://li554-168.members.linode.com/api/'
		},
		settings:{
			lang: "ar",
			langName: "عربي"
		},
		vars:{
			lang:{
				ar: 'ar',
				arName: 'عربي',
				en: 'en',
				enName: 'English'
			},
			testTitle: {
				ar: 'برنامج اختبار الالوان',
				en: 'Color Q Test'
			},
			exit:{
				ar: 'خروج',
				en: 'Logout'
			}
		},
		logedInUser: false
	}
})

// CONFIG
.config(['$routeProvider','$locationProvider','$provide', function($routeProvider, $locationProvider,$provide) {
	// console.log('message');
	$routeProvider
	.when('/', {
		templateUrl: 'temp/login.html',
		controller: 'loginCtrl'
	})
	.when('/q/1', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q1'
	})
	.when('/q/2', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q2'
	})
	.when('/q/3', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q3'
	})
	.when('/q/4', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q4'
	})
	.when('/q/5', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q5'
	})
	.when('/q/6', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q6'
	})
	.when('/q/7', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q7'
	})
	.when('/q/8', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q8'
	})
	.when('/q/9', {
		templateUrl: 'temp/qtemp.html',
		controller: 'q9'
	})
	.when('/q/10', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q10'
	})
	.when('/q/11', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q11'
	})
	.when('/q/12', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q12'
	})
	.when('/q/13', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q13'
	})
	.when('/q/14', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q14'
	})
	.when('/q/15', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q15'
	})
	.when('/q/16', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q16'
	})
	.when('/q/17', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q17'
	})
	.when('/q/18', {
		templateUrl: 'temp/qtemp-2.html',
		controller: 'q18'
	})
	.when('/q/19', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q19'
	})
	.when('/q/20', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q20'
	})
	.when('/q/21', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q21'
	})
	.when('/q/22', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q22'
	})
	.when('/q/23', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q23'
	})
	.when('/q/24', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q24'
	})
	.when('/q/25', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q25'
	})
	.when('/q/26', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q26'
	})
	.when('/q/27', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q27'
	})
	.when('/q/28', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q28'
	})
	.when('/q/29', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q29'
	})
	.when('/q/30', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q30'
	})
	.when('/q/31', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q31'
	})
	.when('/q/32', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q32'
	})
	.when('/q/33', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q33'
	})
	.when('/q/34', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q34'
	})
	.when('/q/35', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q35'
	})
	.when('/q/36', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q36'
	})
	.when('/q/37', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q37'
	})
	.when('/q/38', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q38'
	})
	.when('/q/39', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q39'
	})
	.when('/q/40', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q40'
	})
	.when('/q/41', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q41'
	})
	.when('/q/42', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q42'
	})
	.when('/q/43', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q43'
	})
	.when('/q/44', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q44'
	})
	.when('/q/45', {
		templateUrl: 'temp/qtemp-3.html',
		controller: 'q45'
	})
	.when('/q/46', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q46'
	})
	.when('/q/47', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q47'
	})
	.when('/q/48', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q48'
	})
	.when('/q/49', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q49'
	})
	.when('/q/50', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q50'
	})
	.when('/q/51', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q51'
	})
	.when('/q/52', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q52'
	})
	.when('/q/53', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q53'
	})
	.when('/q/54', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q54'
	})
	.when('/q/55', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q55'
	})
	.when('/q/56', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q56'
	})
	.when('/q/57', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q57'
	})
	.when('/q/58', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q58'
	})
	.when('/q/59', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q59'
	})
	.when('/q/60', {
		templateUrl: 'temp/qtemp-4.html',
		controller: 'q60'
	})
	.when('/result', {
		templateUrl: 'temp/result.html',
		controller: 'resultCtrl'
	})
	.when('/about', {
		templateUrl: 'temp/about.html',
		controller: 'aboutCtrl'
	})
	.when('/404', {
		template: '<h2 style="color:red">Sorry, could not find what your are looking for!</h2> <p>Please try to go back or start from here again <a href="#/">Color Q Test</a></p>',
		controller: function(){
			console.log('Report 404');
		}
	})
	.otherwise({
		redirectTo: '/404'
	});


	// configure html5 to get links working on jsfiddle
	// $locationProvider.html5Mode(true);

		$provide.decorator('$controller', ['$delegate',function ($delegate) {

			return function(constructor, locals) {
					if (typeof constructor == "string") {
						locals.$scope.controllerName =  constructor;
					}
					return $delegate(constructor, locals);
			}

		}]);

}])
/*app.config(['$provide', function ($provide) {
	$provide.decorator('$controller', [
		'$delegate',
		function ($delegate) {
			return function(constructor, locals) {
				if (typeof constructor == "string") {
					locals.$scope.controllerName =  constructor;
				}
				return $delegate(constructor, locals);
			}
		}]);
}]);*/
// RUN
.run(['AppSettings','$rootScope','Questions', function(AppSettings,$rootScope,Questions) {
	// At this point none angular or or app modules are created yet.
	// Adding all the settings propretys

	AppSettings['logedInUser']={
		name: 'Abdullah Alotaibi',
		email: 'as@as.com',
		id: '121232',
	};

	$rootScope.lang = AppSettings.settings.lang;
	$rootScope.langName = 'English';
	console.log(AppSettings);

	$rootScope.currentStep = Questions.step();
	$rootScope.allQestions = Questions.all();

	$rootScope.menuStatus = false;
	$rootScope.triggerMenu = function(){
		$rootScope.menuStatus = !$rootScope.menuStatus;
	}

	$rootScope.changeLang = function(){
		if (AppSettings.settings.lang == 'ar') {
			$rootScope.lang = AppSettings.settings.lang = 'en';
			$rootScope.langName = AppSettings.settings.langName = 'عربي';
		}else{
			$rootScope.lang = AppSettings.settings.lang = 'ar';
			$rootScope.langName = AppSettings.settings.langName = 'English';
		}
		console.log($rootScope.langName, AppSettings.settings.langName);
		return false;
	}

}])

// Factories
.factory('Questions', [ 'AppSettings', function(AppSettings) {

	var current = 0;

	var q =
{
	"g1": [
		{
			"id": "1",
			"ar": {
				"q": "أعتقد  أنه من المهم أكثر",
				"opts": {
					"two":{
						"txt": "أن أكون على صواب",
						"color": ""
					},
					"one":{
						"txt": "أن آتي  بأفكار جديدة",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I think it is more important to",
				"opts": {
					"two":{
						"txt": "Be accurate",
						"color": ""
					},
					"one":{
						"txt": "Come up with new ideas",
						"color": ""
					}
				}
			}
		},
		{
			"id": "3",
			"ar": {
				"q": "عادة أحب صحبة الناس الذين",
				"opts": {
					"two":{
						"txt": "يعبرون ببساطة ووضوح عن أفكارهم",
						"color": ""
					},
					"one":{
						"txt": "لديهم طرق غير معتادة في التعبير عن أفكارهم",
						"color": ""
					}
				}
			},
			"en": {
				"q": "Usually I like to be with people who...",
				"opts": {
					"two":{
						"txt": "Speak plainly",
						"color": ""
					},
					"one":{
						"txt": "have unusual ways of expressing their thoughts",
						"color": ""
					}
				}
			}
		},
		{
			"id": "4",
			"ar": {
				"q": "أنا أفضل التركيز على",
				"opts": {
					"two":{
						"txt": "الحاضر – وما يحدث حالياً",
						"color": ""
					},
					"one":{
						"txt": "المستقبل  – وما يمكن أن يحدث فيه",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I prefer to focus on",
				"opts": {
					"two":{
						"txt": "The present - what is going on",
						"color": ""
					},
					"one":{
						"txt": "Future possibilities - what could happen",
						"color": ""
					}
				}
			}
		},
		{
			"id": "5",
			"ar": {
				"q": "الأشخاص الذين يعرفونني جيدا، يقولون أنني",
				"opts": {
					"two":{
						"txt": "عملي – أرى ماهو قائم",
						"color": ""
					},
					"one":{
						"txt": "خيالي – سريع في تصور الاحتمالات الجديدة",
						"color": ""
					}
				}
			},
			"en": {
				"q": "People who know me well, say I am more",
				"opts": {
					"two":{
						"txt": "Practical - see what is",
						"color": ""
					},
					"one":{
						"txt": "Imaginative - quick to see new possibilities",
						"color": ""
					}
				}
			}
		},
		{
			"id": "6",
			"ar": {
				"q": "في معظم المواقف، أميل إلى",
				"opts": {
					"two":{
						"txt": "تذكر تفاصيل كثيرة",
						"color": ""
					},
					"one":{
						"txt": "أن أكون غير واضح بشأن التفاصيل ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "In most situations, I tend to",
				"opts": {
					"two":{
						"txt": "remember many details",
						"color": ""
					},
					"one":{
						"txt": "be vague about details",
						"color": ""
					}
				}
			}
		},
		{
			"id": "8",
			"ar": {
				"q": "أميل أكثر إلى التفكير",
				"opts": {
					"one":{
						"txt": "بطريقة عملية  ملموسة –  شخص واقعي",
						"color": ""
					},
					"two":{
						"txt": "بطريقة نظرية مجردة -  أركز على عالم الأفكار",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I tend to be more of a",
				"opts": {
					"one":{
						"txt": "Concrete thinker - real world person",
						"color": ""
					},
					"two":{
						"txt": "Abstract thinker focused on the world of ideas",
						"color": ""
					}
				}
			}
		},
		{
			"id": "9",
			"ar": {
				"q": "عادة، أثق",
				"opts": {
					"two":{
						"txt": "بالحقائق",
						"color": ""
					},
					"one":{
						"txt": "بالحدس  تجاه الأشياء ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "Usually, I am more trusting of",
				"opts": {
					"two":{
						"txt": "The facts",
						"color": ""
					},
					"one":{
						"txt": "My intuition",
						"color": ""
					}
				}
			}
		},
		{
			"id": "12",
			"ar": {
				"q": "أنا أكثر اهتماما بــــ",
				"opts": {
					"one":{
						"txt": "الوقائع – ماهو حقيقي",
						"color": ""
					},
					"two":{
						"txt": " بالاحتمالات – مايمكن أن يحدث",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I am more interested in",
				"opts": {
					"one":{
						"txt": "Realities - what is real",
						"color": ""
					},
					"two":{
						"txt": "Possibilities - what could be",
						"color": ""
					}
				}
			}
		},
		{
			"id": "14",
			"ar": {
				"q": "تروق لي الكلمات التالية",
				"opts": {
					"one":{
						"txt": "ينتج – يبني – يعمل",
						"color": ""
					},
					"two":{
						"txt": "يصمم – يبدع - يخترع",
						"color": ""
					}
				}
			},
			"en": {
				"q": "the following words are more appealing to me ",
				"opts": {
					"one":{
						"txt": "Producing, building, and making",
						"color": ""
					},
					"two":{
						"txt": "Designing, creating,  and inventing",
						"color": ""
					}
				}
			}
		}
	],
	"g2": [
		{
			"id": "16",
			"ar": {
				"q": "أميل  مع الآخرين ،  لأن أكون",
				"opts": {
					"one":{
						"txt": "صريحاً  ومباشراً",
						"color": ""
					},
					"two":{
						"txt": "لبقاً ومجاملاً   ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "with other people I tend to be more",
				"opts": {
					"one":{
						"txt": "Frank and Direct",
						"color": ""
					},
					"two":{
						"txt": "Tactful and Diplomatic",
						"color": ""
					}
				}
			}
		},
		{
			"id": "19",
			"ar": {
				"q": "يصفني الناس بأنني",
				"opts": {
					"one":{
						"txt": "عادل وواقعي التفكير",
						"color": ""
					},
					"two":{
						"txt": "رحيم وحنون ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "People describe me as more",
				"opts": {
					"one":{
						"txt": "Just and Tough-Minded",
						"color": ""
					},
					"two":{
						"txt": "Merciful and Tender hearted",
						"color": ""
					}
				}
			}
		},
		{
			"id": "20",
			"ar": {
				"q": "يقدم الناس أداء أفضل عندما",
				"opts": {
					"one":{
						"txt": "يعرفون الأنظمة والقوانين",
						"color": ""
					},
					"two":{
						"txt": "يعرفون شخصاً ما يمنحهم الاهتمام",
						"color": ""
					}
				}
			},
			"en": {
				"q": "People do better when they",
				"opts": {
					"one":{
						"txt": "Know the rules",
						"color": ""
					},
					"two":{
						"txt": "know someone cares about them",
						"color": ""
					}
				}
			}
		},
		{
			"id": "23",
			"ar": {
				"q": "عندما يحدث نزاع ، فأنني غالباً",
				"opts": {
					"one":{
						"txt": "أواجه الطرف الآخر",
						"color": ""
					},
					"two":{
						"txt": "اتجنب النزاع  بقدر مايمكن ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "When there is a conflict, I am more likely to",
				"opts": {
					"one":{
						"txt": "meet it head on",
						"color": ""
					},
					"two":{
						"txt": "avoid dealing with the conflict as long as possible",
						"color": ""
					}
				}
			}
		},
		{
			"id": "26",
			"ar": {
				"q": "إذا انتقدني شخص ما ،  فأنا أميل إلى",
				"opts": {
					"one":{
						"txt": "جعل هذا النقد  لايزعجني",
						"color": ""
					},
					"two":{
						"txt": "أجد صعوبة في عدم أخذ الأمر بصفة شخصية",
						"color": ""
					}
				}
			},
			"en": {
				"q": "If someone criticizes me, I tend to",
				"opts": {
					"one":{
						"txt": "not let it bother me",
						"color": ""
					},
					"two":{
						"txt": "have a hard time not taking it personally",
						"color": ""
					}
				}
			}
		},
		{
			"id": "27",
			"ar": {
				"q": "أصف نفسي بأنني",
				"opts": {
					"one":{
						"txt": "صريح وغير متحيز",
						"color": ""
					},
					"two":{
						"txt": "أراعي حقوق ومشاعر الآخرين ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I would describe myself as more",
				"opts": {
					"one":{
						"txt": "Candid and impartial",
						"color": ""
					},
					"two":{
						"txt": "Considerate of others",
						"color": ""
					}
				}
			}
		},
		{
			"id": "28",
			"ar": {
				"q": "أميل لأن أكون",
				"opts": {
					"one":{
						"txt": "منافس",
						"color": ""
					},
					"two":{
						"txt": "داعم ومساند",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I tend to be more",
				"opts": {
					"one":{
						"txt": "Competitive",
						"color": ""
					},
					"two":{
						"txt": "Supportive",
						"color": ""
					}
				}
			}
		},
		{
			"id": "29",
			"ar": {
				"q": "أعتقد أنه من المهم",
				"opts": {
					"one":{
						"txt": "أن أكون على صواب",
						"color": ""
					},
					"two":{
						"txt": "أن اجعل أصدقائي سعداء ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I think its more important to",
				"opts": {
					"one":{
						"txt": "be right",
						"color": ""
					},
					"two":{
						"txt": "make my friends happy",
						"color": ""
					}
				}
			}
		},
		{
			"id": "30",
			"ar": {
				"q": "يحفزني أكثر",
				"opts": {
					"one":{
						"txt": "الإنجاز",
						"color": ""
					},
					"two":{
						"txt": "التقدير",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I am most motivated by",
				"opts": {
					"one":{
						"txt": "Achievement",
						"color": ""
					},
					"two":{
						"txt": "Appreciation",
						"color": ""
					}
				}
			}
		}
	],
	"g3": [
		{
			"id": "31",
			"ar": {
				"q": "في أغلب الأحيان، أحب أن",
				"opts": {
					"one":{
						"txt": "أضع خططاً  وألتزم بها",
						"color": ""
					},
					"two":{
						"txt": "أجعل الخطط مرنة  لكي يتم تغييرها",
						"color": ""
					}
				}
			},
			"en": {
				"q": "Most of the time, I like to",
				"opts": {
					"one":{
						"txt": "make and stick with plans",
						"color": ""
					},
					"two":{
						"txt": "keep plans flexible so they can be changed",
						"color": ""
					}
				}
			}
		},
		{
			"id": "33",
			"ar": {
				"q": "عندما أقوم بتنفيذ المهمة  المسندة  إلى، أميل لأن",
				"opts": {
					"one":{
						"txt": "أنتهي مبكراً",
						"color": ""
					},
					"two":{
						"txt": "انتهي في الدقيقة الأخيرة ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "when doing my assignment I tend to",
				"opts": {
					"one":{
						"txt": "finish early",
						"color": ""
					},
					"two":{
						"txt": "finish at the last minute",
						"color": ""
					}
				}
			}
		},
		{
			"id": "35",
			"ar": {
				"q": "أحب أن",
				"opts": {
					"one":{
						"txt": "يتم تقرير الأشياء لي من الآخرين",
						"color": ""
					},
					"two":{
						"txt": "أن أمتلك الخيارات في اتخاذ القرار",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I like",
				"opts": {
					"one":{
						"txt": "having things decided for me",
						"color": ""
					},
					"two":{
						"txt": "having choices",
						"color": ""
					}
				}
			}
		},


		{
			"id": "39",
			"ar": {
				"q": "اشعر بالإحباط من الناس الذين",
				"opts": {
					"one":{
						"txt": "يفتقرون للنظام",
						"color": ""
					},
					"two":{
						"txt": "يفتقرون للمرونة",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I get frustrated with those who",
				"opts": {
					"one":{
						"txt": "lack structure",
						"color": ""
					},
					"two":{
						"txt": "lack flexibility",
						"color": ""
					}
				}
			}
		},
		{
			"id": "41",
			"ar": {
				"q": "أفضل الأشخاص  الذين",
				"opts": {
					"one":{
						"txt": "يخبروني ماذا أعمل لاحقاً",
						"color": ""
					},
					"two":{
						"txt": "يتركوني لي أن اختار ماأعمل  لاحقاً ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I prefer those who",
				"opts": {
					"one":{
						"txt": "tell you what to do next",
						"color": ""
					},
					"two":{
						"txt": "let you chose what to do next ",
						"color": ""
					}
				}
			}
		},
		{
			"id": "42",
			"ar": {
				"q": "من الأفضل للمشروع أن يكون",
				"opts": {
					"one":{
						"txt": "منظم",
						"color": ""
					},
					"two":{
						"txt": "جديد و العمل فيه ممتع",
						"color": ""
					}
				}
			},
			"en": {
				"q": "It is better for a project to be",
				"opts": {
					"one":{
						"txt": "organized",
						"color": ""
					},
					"two":{
						"txt": "new and fun to work on",
						"color": ""
					}
				}
			}
		},
		{
			"id": "43",
			"ar": {
				"q": "أفضل أن",
				"opts": {
					"one":{
						"txt": "أعرف ماذا سيحدث لاحقاً",
						"color": ""
					},
					"two":{
						"txt": "أجرب أشياء جديدة",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I prefer to ",
				"opts": {
					"one":{
						"txt": "know what will happen next",
						"color": ""
					},
					"two":{
						"txt": "try new things",
						"color": ""
					}
				}
			}
		},
		{
			"id": "44",
			"ar": {
				"q": "أراني  أكثر ارتياحاً  عند",
				"opts": {
					"one":{
						"txt": "إنهاء مشروع ما",
						"color": ""
					},
					"two":{
						"txt": "البدء  في مشروع ما",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I find it more satisfying to",
				"opts": {
					"one":{
						"txt": "finish a project",
						"color": ""
					},
					"two":{
						"txt": "start a project",
						"color": ""
					}
				}
			}
		},
		{
			"id": "45",
			"ar": {
				"q": "عندما يكون لدي موعدا فأنني غالبا",
				"opts": {
					"one":{
						"txt": "أحافظ على الحضور في الوقت المحدد  أو مبكرا",
						"color": ""
					},
					"two":{
						"txt": "أتأخر  أو أصل في أخر دقيقة",
						"color": ""
					}
				}
			},
			"en": {
				"q": "when I have an appointment I am often",
				"opts": {
					"one":{
						"txt": "punctual and even early",
						"color": ""
					},
					"two":{
						"txt": "late or arrive at the last minute",
						"color": ""
					}
				}
			}
		}
	],
	"g4": [
		{
			"id": "47",
			"ar": {
				"q": "عادة أفضل أن",
				"opts": {
					"one":{
						"txt": "ألتقي و أتحدث مع أشخاص كثيرين",
						"color": ""
					},
					"two":{
						"txt": "ألتقي وأتحدث مع قليل من الأشخاص",
						"color": ""
					}
				}
			},
			"en": {
				"q": "Usually I prefer to ",
				"opts": {
					"one":{
						"txt": "meet and talk with many people",
						"color": ""
					},
					"two":{
						"txt": "meet and talk with few people",
						"color": ""
					}
				}
			}
		},
		{
			"id": "48",
			"ar": {
				"q": "أستمتع",
				"opts": {
					"one":{
						"txt": "بإجراء حديث قصير مع الغرباء",
						"color": ""
					},
					"two":{
						"txt": "بملاحظة حديث الآخرين",
						"color": ""
					}
				}
			},
			"en": {
				"q": "I enjoy",
				"opts": {
					"one":{
						"txt": "making small talk with strangers",
						"color": ""
					},
					"two":{
						"txt": "observing the conversation of others",
						"color": ""
					}
				}
			}
		},
		{
			"id": "50",
			"ar": {
				"q": "عندما أكون مع مجموعة من  الأصدقاء، فإنني",
				"opts": {
					"one":{
						"txt": "أتحدث  أكثر من الآخرين",
						"color": ""
					},
					"two":{
						"txt": "أتحدث  أقل من الآخرين",
						"color": ""
					}
				}
			},
			"en": {
				"q": "When I am with group of friends, I ",
				"opts": {
					"one":{
						"txt": "say more than others",
						"color": ""
					},
					"two":{
						"txt": "say less than others",
						"color": ""
					}
				}
			}
		},
		{
			"id": "52",
			"ar": {
				"q": "أفضل أن أتعلم عن موضوع جديد من خلال",
				"opts": {
					"one":{
						"txt": "الاستماع إلى نقاش عنه",
						"color": ""
					},
					"two":{
						"txt": "قراءة كتاب عنه",
						"color": ""
					}
				}
			},
			"en": {
				"q": "It is more important for me to have",
				"opts": {
					"one":{
						"txt": "a wide circle of friends",
						"color": ""
					},
					"two":{
						"txt": "a few close friends",
						"color": ""
					}
				}
			}
		},
		{
			"id": "53",
			"ar": {
				"q": "في اللقاءات الاجتماعية ، أنا على الأرجح  أميل أكثر",
				"opts": {
					"one":{
						"txt": "للاقتراب  من  الآخرين",
						"color": ""
					},
					"two":{
						"txt": "للانتظار حتى يقترب مني  الآخرين",
						"color": ""
					}
				}
			},
			"en": {
				"q": "At a social gathering, I am more likely to",
				"opts": {
					"one":{
						"txt": "approach people",
						"color": ""
					},
					"two":{
						"txt": "wait to be approached",
						"color": ""
					}
				}
			}
		},
		{
			"id": "55",
			"ar": {
				"q": "عادة  أتبادل معلوماتي الشخصية مع الآخرين",
				"opts": {
					"one":{
						"txt": "بسهولة",
						"color": ""
					},
					"two":{
						"txt": "بتردد ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "Usually I",
				"opts": {
					"one":{
						"txt": "share personal  information easily",
						"color": ""
					},
					"two":{
						"txt": "am reluctant to share personal information",
						"color": ""
					}
				}
			}
		},
		{
			"id": "56",
			"ar": {
				"q": "في النقاش ، غالباً",
				"opts": {
					"one":{
						"txt": "أتحدث قبل أن أفكر",
						"color": ""
					},
					"two":{
						"txt": "أتمعن في الموضوع جيدا قبل أن أتحدث",
						"color": ""
					}
				}
			},
			"en": {
				"q": "In a discussion I often",
				"opts": {
					"one":{
						"txt": "speak before I think",
						"color": ""
					},
					"two":{
						"txt": "consider the issue carefully before I speak ",
						"color": ""
					}
				}
			}
		},
		{
			"id": "57",
			"ar": {
				"q": "يصفني الآخرون بأنني",
				"opts": {
					"one":{
						"txt": "وديّ",
						"color": ""
					},
					"two":{
						"txt": "متحفظ  ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "People would describe me as more",
				"opts": {
					"one":{
						"txt": "outgoing ",
						"color": ""
					},
					"two":{
						"txt": "reserved",
						"color": ""
					}
				}
			}
		},
		{
			"id": "59",
			"ar": {
				"q": "عندما أكون بمفردي لفترة طويلة  ، فإنني",
				"opts": {
					"one":{
						"txt": "أصبح قلقاً",
						"color": ""
					},
					"two":{
						"txt": "أشعر بالارتياح ",
						"color": ""
					}
				}
			},
			"en": {
				"q": "when I am alone for a long period of time, I ",
				"opts": {
					"one":{
						"txt": "become restless",
						"color": ""
					},
					"two":{
						"txt": "feel rested",
						"color": ""
					}
				}
			}
		}
	]
};
	/*[
		{
			"ar":{
				"q": "أعتقد  أنه من المهم أكثر",
				"opts":{
					"one":"أن أكون على صواب",
					"two":"أن آتي  بأفكار جديدة"
				}
			},
			"en":{
				"q":"I think it is more important to",
				"opts":{
					"one":"Come up with new ideas",
					"two":"Be accurate"
				}
			}
		},{
			"ar":{
				"q": "يقدرني الناس أكثر على",
				"opts":{
					"one":"فطرتي السليمة",
					"two":"إبداعي "
				}
			},
			"en":{
				"q": "People value me most for...",
				"opts":{
					"one":"My creativity",
					"two":"My common sense"
				}
			}
		},{
			"ar":{
				"q": "عادة أحب صحبة الناس الذين",
				"opts":{
					"one":"يعبرون ببساطة ووضوح عن أفكارهم",
					"two":"لديهم طرق غير معتادة في التعبير عن أفكارهم"
				}
			},
			"en":{
				"q": "Usually I like to be with people who...",
				"opts":{
					"one":"have unusual ways of expressing their thoughts",
					"two":"Speak plainly"
				}
			}
		},{
			"ar":{
				"q": "أنا أفضل التركيز على",
				"opts":{
					"one":"الحاضر – وما يحدث حالياً",
					"two":"المستقبل  – وما يمكن أن يحدث فيه"
				}
			},
			"en":{
				"q": "I prefer to focus on",
				"opts":{
					"one":"Future Possibilities",
					"two":"The present"
				}
			}
		},{
			"ar":{
				"q": "الأشخاص الذين يعرفونني جيدا، يقولون أنني",
				"opts":{
					"one":"عملي – أرى ماهو قائم",
					"two":"خيالي – سريع في تصور الاحتمالات الجديدة"
				}
			},
			"en":{
				"q": "People who know me well, say I am more",
				"opts":{
					"one":"Imaginative",
					"two":"Practical"
				}
			}
		},{
			"ar":{
				"q": "في معظم المواقف، أميل إلى",
				"opts":{
					"one":"تذكر تفاصيل كثيرة",
					"two":"أن أكون غير واضح بشأن التفاصيل "
				}
			},
			"en":{
				"q": "In most situations, I tend to...",
				"opts":{
					"one":"be vague about details",
					"two":"remember many details"
				}
			}
		},{
			"ar":{
				"q": "من الصعب علي  أن أفهم الأشخاص الذين",
				"opts":{
					"one":"ليس لديهم فطرة سليمة وبديهية في الحكم على الأشياء",
					"two":"ليس لديهم رؤية واضحة "
				}
			},
			"en":{
				"q": "It is hard for me to understand people who...",
				"opts":{
					"one":"Have no vision",
					"two":"Have no common sense"
				}
			}
		},{
			"ar":{
				"q": "أميل أكثر إلى التفكير",
				"opts":{
					"one":"بطريقة عملية  ملموسة –  شخص واقعي",
					"two":"بطريقة نظرية مجردة -  أركز على عالم الأفكار"
				}
			},
			"en":{
				"q": "I tend to be more a...",
				"opts":{
					"one":"Abstract thinker",
					"two":"Concrete thinker"
				}
			}
		},{
			"ar":{
				"q": "عادة، أثق",
				"opts":{
					"one":"بالحقائق",
					"two":"بالحدس  تجاه الأشياء "
				}
			},
			"en":{
				"q": "Usually, I am more trusting of",
				"opts":{
					"one":"My intuition",
					"two":"The facts"
				}
			}
		},{
			"ar":{
				"q": "أفضل أن",
				"opts":{
					"one":"أعمل شئ ما  حسب ماتعلمته",
					"two":"ابحث  عن  طرق جديدة لعمل الأشياء"
				}
			},
			"en":{
				"q": "I would rather",
				"opts":{
					"one":"Find a new way to do things",
					"two":"Do something the way I was taught"
				}
			}
		},{
			"ar":{
				"q": "أفضل أن أصاحب شخصاً",
				"opts":{
					"one":"تقليدي",
					"two":"يأتي  دوما بأفكار جديدة "
				}
			},
			"en":{
				"q": "I would rather have as a friend someone who",
				"opts":{
					"one":"has both feet on the ground",
					"two":"is always coming up with new ideas"
				}
			}
		},{
			"ar":{
				"q": "أنا أكثر اهتماما بــــ",
				"opts":{
					"one":"الوقائع – ماهو حقيقي",
					"two":" بالاحتمالات – مايمكن أن يحدث"
				}
			},
			"en":{
				"q": "I am more interested in",
				"opts":{
					"one":"Realities",
					"two":"Possibilities"
				}
			}
		},{
			"ar":{
				"q": "أفضل أن",
				"opts":{
					"one":"أعمل",
					"two":"أتخيل  "
				}
			},
			"en":{
				"q": "I prefer to be",
				"opts":{
					"one":"Doing",
					"two":"Imagining"
				}
			}
		},{
			"ar":{
				"q": "تروق لي الكلمات التالية",
				"opts":{
					"one":"ينتج – يبني – يعمل",
					"two":"يصمم – يبدع - يخترع"
				}
			},
			"en":{
				"q": "the following words are more appealing to me ",
				"opts":{
					"one":"Producing, building, and making",
					"two":"Designing, creating,  and inventive"
				}
			}
		},{
			"ar":{
				"q": "يقول الناس أنني أفضل",
				"opts":{
					"one":"حل المشكلات العملية",
					"two":" إيجاد حلول مبتكرة للمشكلات "
				}
			},
			"en":{
				"q": "people say I am best at",
				"opts":{
					"one":"Solving practical problems",
					"two":"Finding innovative solutions to problems"
				}
			}
		},{
			"ar":{
				"q": "أميل  مع الآخرين ،  لأن أكون",
				"opts":{
					"one":"صريحاً  ومباشراً",
					"two":"لبقاً ومجاملاً   "
				}
			},
			"en":{
				"q": "with other people I tend to be more",
				"opts":{
					"one":"Frank and Direct",
					"two":"Tactful and Diplomatic"
				}
			}
		},{
			"ar":{
				"q": "أنا فخور  بقدراتي  على",
				"opts":{
					"one":"تحليل المشكلات بشكل جيد",
					"two":"تقديم خدمات للآخرين "
				}
			},
			"en":{
				"q": "I am most proud of my ability  ",
				"opts":{
					"one":"Analyze problems well",
					"two":"Be service of others"
				}
			}
		},{
			"ar":{
				"q": "أساهم   في مساعدة الآخرين على النحو الأفضل من خلال",
				"opts":{
					"one":"تقديم تحليل منطقي لهم",
					"two":"التعبير عن تعاطفي  معهم حول مايقلقهم "
				}
			},
			"en":{
				"q": "I contribute to others best by",
				"opts":{
					"one":"Providing logical analysis",
					"two":"Expressing Sympathy"
				}
			}
		},{
			"ar":{
				"q": "يصفني الناس بأنني",
				"opts":{
					"one":"عادل وواقعي التفكير",
					"two":"رحيم وحنون "
				}
			},
			"en":{
				"q": "People Describe me as more",
				"opts":{
					"one":"Just and Tough-Minded",
					"two":"Merciful and Tender hearted"
				}
			}
		},{
			"ar":{
				"q": "يقدم الناس أداء أفضل عندما",
				"opts":{
					"one":"يعرفون الأنظمة والقوانين",
					"two":"يعرفون شخصاً ما يمنحهم الاهتمام "
				}
			},
			"en":{
				"q": "Students do better when they",
				"opts":{
					"one":"Know the rules",
					"two":"know someone cares about them"
				}
			}
		},{
			"ar":{
				"q": "أفضل الناس الذين هم",
				"opts":{
					"one":"منصفون دائماً",
					"two":"لطفاء دائماً "
				}
			 },
			"en":{
				"q": "I prefer teachers who are",
				"opts":{
					"one":"Always fair",
					"two":"Always kind"
				}
			}
		},{
			"ar":{
				"q": "اتحمس جداً",
				"opts":{
					"one":"لمعرفة عمل الأشياء",
					"two":"لإيجاد طرق لمساعدة الآخرين"
				}
			},
			"en":{
				"q": "I get most excited by",
				"opts":{
					"one":"Figuring out how things work",
					"two":"Finding ways of helping other people"
				}
			}
		},{
			"ar":{
				"q": "عندما يحدث نزاع ، فأنني غالباً",
				"opts":{
					"one":"أواجه الطرف الآخر",
					"two":"اتجنب النزاع  بقدر مايمكن "
				}
			},
			"en":{
				"q": "When there is a conflict, I am more likely to",
				"opts":{
					"one":"meet it head on",
					"two":"avoid dealing with the conflict as long as possible"
				}
			}
		},{
			"ar":{
				"q": "أفضل أن يعرفني الآخرين بأنني",
				"opts":{
					"one":"شخص عقلاني  جداً",
					"two":"شخص عاطفي "
				}
			 },
			"en":{
				"q": "I would rather be known as a",
				"opts":{
					"one":"very logical person",
					"two":"very compassionate person"
				}
			}
		},{
			"ar":{
				"q": "عندما أفوز في لعبة  أو مسابقة ما  ، فإنني",
				"opts":{
					"one":"أشعر بحلاوة الفوز",
					"two":"أشعر بحلاوة الفوز ، و لكن أقلق بشأن ما يشعر به الخاسرون"
				}
			},
			"en":{
				"q": "When I win in a game or competition, I",
				"opts":{
					"one":"feel great only",
					"two":"feel great but worry about how the losers feel"
				}
			}
		},{
			"ar":{
				"q": "إذا انتقدني شخص ما ،  فأنا أميل إلى",
				"opts":{
					"one":"جعل هذا النقد  لايزعجني",
					"two":"أجد صعوبة في عدم أخذ الأمر بصفة شخصية "
				}
			},
			"en":{
				"q": "If someone criticizes me, i tend to ",
				"opts":{
					"one":"not let it bother me",
					"two":"have a hard time not taking it personally"
				}
			}
		},{
			"ar":{
				"q": "أصف نفسي بأنني",
				"opts":{
					"one":"صريح وغير متحيز",
					"two":"أراعي حقوق ومشاعر الآخرين "
				}
			},
			"en":{
				"q": "I would describe myself as more",
				"opts":{
					"one":"Candid and impartial",
					"two":"Considerate of others"
				}
			}
		},{
			"ar":{
				"q": "أميل لأن أكون",
				"opts":{
					"one":"منافس",
					"two":"داعم ومساند  "
				}
			},
			"en":{
				"q": "I tend to be more",
				"opts":{
					"one":"Competitive",
					"two":"Supportive"
				}
			}
		},{
			"ar":{
				"q": "أعتقد أنه من المهم",
				"opts":{
					"one":"أن أكون على صواب",
					"two":"أن اجعل أصدقائي سعداء "
				}
			},
			"en":{
				"q": "I think its more important to ",
				"opts":{
					"one":"be right",
					"two":"make my friends happy"
				}
			}
		},{
			"ar":{
				"q": "يحفزني أكثر",
				"opts":{
					"one":"الإنجاز",
					"two":"التقدير"
				}
			},
			"en":{
				"q": "I am most motivated by",
				"opts":{
					"one":"Achievement",
					"two":"Appreciation"
				}
			}
		},{
			"ar":{
				"q": "في أغلب الأحيان، أحب أن",
				"opts":{
					"one":"أضع خططاً  وألتزم بها",
					"two":"أجعل الخطط مرنة  لكي يتم تغييرها "
				}
			},
			"en":{
				"q": "Most of the time, i like to",
				"opts":{
					"one":"make and stick with plans",
					"two":"keep plans flexible so they can be changed"
				}
			}
		},{
			"ar":{
				"q": "أفضل أن أمتلك",
				"opts":{
					"one":"جدولاً للعمل",
					"two":"الفرصة للعمل بعفوية ً  "
				}
			},
			"en":{
				"q": "I prefer to have",
				"opts":{
					"one":"a schedule",
					"two":"the opportunity to be spontaneous "
				}
			}
		},{
			"ar":{
				"q": "عندما أقوم بتنفيذ المهمة  المسندة  إلى، أميل لأن",
				"opts":{
					"one":"أنتهي مبكراً",
					"two":"انتهي في الدقيقة الأخيرة  "
				}
			},
			"en":{
				"q": "when doing my assignment i tend to ",
				"opts":{
					"one":"finish early",
					"two":"finish at the last minute "
				}
			}
		},{
			"ar":{
				"q": "أجد أن المقاطعة",
				"opts":{
					"one":"تحول دون العمل بشكل مناسب",
					"two":"تجعل اليوم أكثر متعة وإثارة "
				}
			},
			"en":{
				"q": "I find interruptions",
				"opts":{
					"one":"keep me from working properly",
					"two":"make the day more interesting and exciting "
				}
			}
		},{
			"ar":{
				"q": "أحب أن",
				"opts":{
					"one":"يتم تقرير الأشياء لي من الآخرين",
					"two":"أن أمتلك الخيارات في اتخاذ القرار"
				}
			},
			"en":{
				"q": "I like",
				"opts":{
					"one":"having things decided for me ",
					"two":"having choices"
				}
			}
		},{
			"ar":{
				"q": "عندما أضع الخطط",
				"opts":{
					"one":"أضع حساب لمعظم التفاصيل مسبقاً",
					"two":"أفضل أن أنتظر وأعالج المشكلات عندما تظهر "
				}
			},
			"en":{
				"q": "When i make plans, I ",
				"opts":{
					"one":"work out most details in advance ",
					"two":"prefer to wait and handle problems as they come up "
				}
			}
		},{
			"ar":{
				"q": "يصفني  الناس  بأنني",
				"opts":{
					"one":"منظم ومرتب",
					"two":"مرن وقابل للتكيف  "
				}
			},
			"en":{
				"q": "People describe me as more",
				"opts":{
					"one":"organized and orderly",
					"two":"flexible and adaptable "
				}
			}
		},{
			"ar":{
				"q": "أفضل أن",
				"opts":{
					"one":"أرتب المناسبات الاجتماعية مسبقاً",
					"two":"أكون حراً  لعمل مااشعر به عندما يحين الوقت لذلك "
				}
			},
			"en":{
				"q": "I prefer to",
				"opts":{
					"one":"arrange social events in advance",
					"two":"be free to do what i feel like when the time comes"
				}
			}
		},{
			"ar":{
				"q": "أميل لأن",
				"opts":{
					"one":"أعمل أولاً ثم أستريح وأستمتع بوقتي",
					"two":"أتسلى وأستريح أولاً  لمعرفتي بإنجاز العمل لاحقاً "
				}
			},
			"en":{
				"q": "I get frustrated with teachers who",
				"opts":{
					"one":"lack structure",
					"two":"lack flexibility"
				}
			}
		},{
			"ar":{
				"q": "اشعر بالإحباط من الناس الذين",
				"opts":{
					"one":"يفتقرون للنظام",
					"two":"يفتقرون للمرونة"
				}
			},
			"en":{
				"q": "I tend to ",
				"opts":{
					"one":"work first then relax and have fun",
					"two":"have fun first knowing i will get the work done"
				}
			}
		},{
			"ar":{
				"q": "أفضل الأشخاص  الذين",
				"opts":{
					"one":"يخبروني ماذا أعمل لاحقاً",
					"two":"يتركوني لي أن اختار ماأعمل  لاحقاً "
				}
			},
			"en":{
				"q": "I prefer teachers who",
				"opts":{
					"one":"tell you what to do next",
					"two":"let you chose what to do next "
				}
			}
		},{
			"ar":{
				"q": "من الأفضل للمشروع أن يكون",
				"opts":{
					"one":"منظم",
					"two":"جديد و العمل فيه ممتع "
				}
			},
			"en":{
				"q": "It is better for a project to be ",
				"opts":{
					"one":"organized",
					"two":"new and fun to work on "
				}
			}
		},{
			"ar":{
				"q": "أفضل أن",
				"opts":{
					"one":"أعرف ماذا سيحدث لاحقاً",
					"two":"أجرب أشياء جديدة"
				}
			},
			"en":{
				"q": "I prefer to ",
				"opts":{
					"one":"know what will happen next",
					"two":"try new things"
				}
			}
		},{
			"ar":{
				"q": "أراني  أكثر ارتياحاً  عند",
				"opts":{
					"one":"إنهاء مشروع ما",
					"two":"البدء  في مشروع ما "
				}
			},
			"en":{
				"q": "I find it more satisfying to ",
				"opts":{
					"one":"finish a project",
					"two":"start a project"
				}
			}
		},{
			"ar":{
				"q": "عندما يكون لدي موعدا فأنني غالبا",
				"opts":{
					"one":"أحافظ على الحضور في الوقت المحدد  أو مبكرا",
					"two":"أتأخر  أو أصل في أخر دقيقة "
				}
			},
			"en":{
				"q": "when i have an appointment i am often ",
				"opts":{
					"one":"punctual and even early",
					"two":"late or arrive at the last minute "
				}
			}
		},{
			"ar":{
				"q": "تكون المشاريع الخاصة أكثر متعة عندما",
				"opts":{
					"one":"أعمل مع مجموعة",
					"two":"أعمل  فيها بمفردي "
				}
			},
			"en":{
				"q": "special projects are more fun when i am ",
				"opts":{
					"one":"working with a group",
					"two":"working on my own "
				}
			}
		},{
			"ar":{
				"q": "عادة أفضل أن",
				"opts":{
					"one":"ألتقي و أتحدث مع أشخاص كثيرين",
					"two":"ألتقي وأتحدث مع قليل من الأشخاص "
				}
			 },
			"en":{
				"q": "Usually i prefer to ",
				"opts":{
					"one":"meet and talk with many people ",
					"two":"meet and talk with few people "
				}
			}
		},{
			"ar":{
				"q": "أستمتع",
				"opts":{
					"one":"بإجراء حديث قصير مع الغرباء",
					"two":"بملاحظة حديث الآخرين "
				}
			},
			"en":{
				"q": "I enjoy",
				"opts":{
					"one":"making small talks with strangers",
					"two":"observing the conversation of others"
				}
			}
		},{
			"ar":{
				"q": "يصفني الآخرون بأنني",
				"opts":{
					"one":"متشوق",
					"two":"هادئ "
				}
			},
			"en":{
				"q": "Others would describe me as ",
				"opts":{
					"one":"eager ",
					"two":"calm"
				}
			}
		},{
			"ar":{
				"q": "عندما أكون مع مجموعة من  الأصدقاء، فإنني",
				"opts":{
					"one":"أتحدث  أكثر من الآخرين",
					"two":"أتحدث  أقل من الآخرين"
				}
			},
			"en":{
				"q": "When i am with group of friends, I ",
				"opts":{
					"one":"say more than others",
					"two":"say less than others"
				}
			}
		},{
			"ar":{
				"q": "من المهم أكثر بالنسبة لي أن يكون لدي",
				"opts":{
					"one":"مجموعة كبيرة  من الأصدقاء",
					"two":"عدد قليل من الأصدقاء المقربين "
				}
			},
			"en":{
				"q": "I prefer to learn about a new subject by",
				"opts":{
					"one":"listening to a discussion ",
					"two":"reading a book about it "
				}
			}
		},{
			"ar":{
				"q": "أفضل أن أتعلم عن موضوع جديد من خلال",
				"opts":{
					"one":"الاستماع إلى نقاش عنه",
					"two":"قراءة كتاب عنه"
				}
			},
			"en":{
				"q": "It is more important for me to have ",
				"opts":{
					"one":"a wide circle of friends",
					"two":"a few close friends "
				}
			}
		},{
			"ar":{
				"q": "في اللقاءات الاجتماعية ، أنا على الأرجح  أميل أكثر",
				"opts":{
					"one":"للاقتراب  من  الآخرين",
					"two":"للانتظار حتى يقترب مني  الآخرين  "
				}
			},
			"en":{
				"q": "At a social gathering, I am more likely to ",
				"opts":{
					"one":"approach people",
					"two":"wait to be approached "
				}
			}
		},{
			"ar":{
				"q": "عندما أقوم بعمل واجبي",
				"opts":{
					"one":"أفضل  أن يكون هناك أشخاص من حولي",
					"two":"أفضل العمل بمفردي"
				}
			},
			"en":{
				"q": "When doing my homework ( or work), I ",
				"opts":{
					"one":"prefer to have people around me ",
					"two":"prefer to work alone "
				}
			}
		},{
			"ar":{
				"q": "عادة  أتبادل معلوماتي الشخصية مع الآخرين",
				"opts":{
					"one":"بسهولة",
					"two":"بتردد "
				}
			},
			"en":{
				"q": "Usually I ",
				"opts":{
					"one":"share personal  information easily ",
					"two":"am reluctant to share personal information"
				}
			}
		},{
			"ar":{
				"q": "في النقاش ، غالباً",
				"opts":{
					"one":"أتحدث قبل أن أفكر",
					"two":"أتمعن في الموضوع جيدا قبل أن أتحدث"
				}
			},
			"en":{
				"q": "In a discussion I often ",
				"opts":{
					"one":"speak before i think ",
					"two":"consider the issue carefully before i speak  "
				}
			}
		},{
			"ar":{
				"q": "يصفني الآخرون بأنني",
				"opts":{
					"one":"وديّ",
					"two":"متحفظ  "
				}
			},
			"en":{
				"q": "People would describe me as more",
				"opts":{
					"one":"outgoing ",
					"two":"reserved"
				}
			}
		},{
			"ar":{
				"q": "أجد  تكوين  أصدقاء جدد",
				"opts":{
					"one":"عملية مثيرة",
					"two":"عملية صعبة"
				}
			},
			"en":{
				"q": "I find making new friends ",
				"opts":{
					"one":"exciting ",
					"two":"hard"
				}
			}
		},{
			"ar":{
				"q": "عندما أكون بمفردي لفترة طويلة  ، فإنني",
				"opts":{
					"one":"أصبح قلقاً",
					"two":"أشعر بالارتياح "
				}
			},
			"en":{
				"q": "when I am alone for a long period of time, I ",
				"opts":{
					"one":"become restless",
					"two":"feel rested "
				}
			}
		},{
			"ar":{
				"q": "أفضل",
				"opts":{
					"one":"التنوع والعمل",
					"two":"الهدوء والتأمل"
				}
			},
			"en":{
				"q": "I prefer ",
				"opts":{
					"one":"variety and action ",
					"two":"quiet and reflection "
				}
			}
		}
	];*/

	return{
		get: function(group, Qnumber){
			// console.log( q[ (parseInt(Qn.substring(1))-1) ][AppSettings.settings.lang] );
			/*if (q[ (parseInt(Qn.substring(1))-1) ][AppSettings.settings.lang])
				return q[ (parseInt(Qn.substring(1))-1) ][AppSettings.settings.lang];*/
			return q[ group ][ Qnumber-1 ];
		},
		getCurrent: function(){
			if (q[current][AppSettings.settings.lang])
				return q[current][AppSettings.settings.lang];
		},
		next: function(){
			if (current == q.length)
				return false;

			current+=1;
		},
		prev: function(){
			if (current == 0)
				return false;

			current-=1
		},
		step: function(){
			return current;
		},
		all: function(){
			return q;
		}
	}

}])

// controllers

.controller('loginCtrl', ['$scope','$location', function($scope,$location) {
	$scope.logIn = function(){
		 $location.path('/q/1');
		 // console.log('a');
	}

	/*console.log('login');
	TweenMax.to('.blue', 2, {left:'-385px', repeat:100, delay:0,yoyo:true,repeatDelay:0.5, ease:Linear.easeOut});
	TweenMax.to('.green', 1.5, {left:'-385px', repeat:100,delay:1, yoyo:true,repeatDelay:1, ease:Linear.easeOut});
	TweenMax.to('.red', 1, {left:'-385px', repeat:100,delay:1.5, yoyo:true,repeatDelay:1.5, ease:Linear.easeOut});
	TweenMax.to('.gold', 0.5, {left:'-385px', repeat:100,delay:2, yoyo:true,repeatDelay:2,ease:Linear.easeOut});*/

}])

.controller('q1', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	console.log($scope.test, qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#C7A460';
		$scope.selected = 'opt1';
		// $scope.obj = 'floating';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#F15940';
		$scope.selected = 'opt2';
		// $scope.obj = '';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}

}])

.controller('q2', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';
	$scope.chars = 'q'+ $scope.test.id;
	console.log($scope.chars);

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#A82865';
		$scope.selected = 'opt1';
		// $scope.obj = 'rotate-cw';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#A97C50';
		$scope.selected = 'opt2';
		// $scope.obj = '';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])

.controller('q3', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);

	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#B92C59';
		$scope.selected = 'opt1';
		$scope.head = '';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#639F4A';
		$scope.selected = 'opt2';
		// $scope.head = '';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q4', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#68CCEF';
		$scope.selected = 'opt1';
		// $scope.obj = '';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#662D91';
		$scope.selected = 'opt2';
		// $scope.obj = 'flash';
		// $scope.head = '';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])

.controller('q5', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#FFC20E';
		$scope.selected = 'opt1';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#4E4295';
		$scope.selected = 'opt2';
		// $scope.obj = '';
		// $scope.head = '';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q6', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#91D8F2';
		$scope.selected = 'opt1';
		// $scope.head = 'slide-tree';
		// $scope.obj = '';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#91D8F2';
		$scope.selected = 'opt2';
		// $scope.head = 'slide-tree';
		// $scope.obj = 'slide-tree-obj';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q7', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';
	$scope.chars = 'q'+ $scope.test.id;

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#C12F2A';
		$scope.selected = 'opt1';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#72A233';
		$scope.selected = 'opt2';
		// $scope.obj = 'slide-left';
		// $scope.head = 'rotate-ccw';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q8', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#802881';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#F6921E';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q9', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g1',qNumber);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#B52924';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#FAAF40';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q10', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',1);
	console.log($scope.test);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#9A8479';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#7BC35B';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q11', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',2);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#4DB1CC';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#4DB1CC';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q12', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',3);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#038ACB';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#17636C';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q13', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',4);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#943A86';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#26A9D5';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q14', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',5);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#FCB614';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#47A447';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q15', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',6);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#FCB614';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#26A9D5';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q16', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',7);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#EC297B';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#7BC35B';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q17', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',8);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '#FFCB05';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '#81D1E5';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q18', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g2',9);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q19', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',1);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q20', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',2);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q21', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',3);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q22', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',4);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q23', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',5);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q24', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',6);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q25', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',7);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q26', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',8);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q27', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g3',9);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q28', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',1);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q29', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',2);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q30', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',3);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q31', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);

	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',4);
	$rootScope.bgcolor = '#767777';
	console.log($scope.test);

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q32', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',5);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q33', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',6);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q34', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',7);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q35', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',8);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q36', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	var qNumber = parseInt($scope.controllerName.substring(1));
	$scope.test = Questions.get('g4',9);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = 'q'+$scope.test.id+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = 'q'+$scope.test.id+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q37', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q38', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q39', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q40', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q41', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q42', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q43', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q44', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q45', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q46', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q47', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q48', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q49', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q50', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q51', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q52', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q53', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q54', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q55', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q56', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q57', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q58', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q59', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';
	console.log($scope.test);
	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q60', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);
	$rootScope.bgcolor = '#767777';
	// console.log($scope.test);

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/result' );
	}
}])
.controller('resultCtrl', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = 59;
	$scope.color = 'blue';
	$scope.desc = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus explicabo, atque illo modi eos ullam, ex omnis quos magnam odit!';

}]);





















/*.controller('base', ['$rootScope','$scope','$location','Questions', function($rootScope,$scope,$location,Questions) {
	$rootScope.currentStep = (parseInt($scope.controllerName.substring(1))-1);
	$scope.selected = false;
	$scope.test = Questions.get($scope.controllerName);

	$scope.selectOne = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt1';
		$scope.question = $scope.controllerName+'-opt1';
		return false;
	}
	$scope.selectTwo = function(){
		$rootScope.bgcolor = '';
		$scope.selected = 'opt2';
		$scope.question = $scope.controllerName+'-opt2';
		return false;
	}
	$scope.next = function(){
		Questions.next();
		$location.path('/q/'+(parseInt($scope.controllerName.substring(1))+1) );
	}
}])
.controller('q00', ['$rootScope','$controller','$scope','$location','Questions', function($rootScope,$controller,$scope,$location,Questions) {
	var _this = this;

	// Mixin Animal functionality into Dog.
	angular.extend(this, new AnimalController($scope, 'BARK BARK!', 'solid black', '35mph'));

	// $scope.bark = function () {
		// _this.vocalize(); // inherited from mixin.
	// }
	$scope.as = 's';
	console.log($scope);

}])
.controller('aboutCtrl', ['$scope', function($scope) {
	console.log($scope);
}]);

function AnimalController($scope, vocalization, color, runSpeed) {

	var _this = this;

	// Mixin instance properties.
	this.vocalization = vocalization;
	this.runSpeed = runSpeed;

	// Mixin instance methods.
	this.vocalize = function () {
		console.log(this.vocalization);
	};

	// Mixin scope properties.
	$scope.color = color;

	// Mixin scope methods.
	$scope.run = function(){
		console.log("run speed: " + _this.runSpeed );
	};
}*/







/*setInterval(function(){
	var index = $('.progress li.active').index();
	$('.progress li.active').removeClass('active');
	$('.progress li').eq(index+1).addClass('active');
	console.log(index)
},100);*/










